import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  // Data Local
  STORAGE_LOCAL_TODO_LIST:string = 'todo-list:local-data_lists';
  STORAGE_LOCAL_TODO_LIST_CURRENT:string = 'todo-list:local-data_lists_current';
  STORAGE_LOCAL_TODO_LIST_TASKS:string = 'todo-list:local-data_list_tasks-';

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
  ) { }

  async showToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'bottom',
      buttons: ['OK']
    });
    await toast.present();
  }

  /**
   * Show a loading spinner with an optional message.
   *
   * @param {string} msg - The message to display in the loading spinner, this msg must be [KEY TRANSLATION]. Defaults to 'labels.wait'.
   * @return {Promise<HTMLIonLoadingElement>} A promise that resolves to the loading spinner element.
   */
  async showLoading(msg:string = 'labels.wait'): Promise<HTMLIonLoadingElement> {
    const loader = await this.loadingCtrl.create({
      message: `${msg}...`,
      animated: true,
      cssClass: 'loader-nep'
    });
    await loader.present();
    return loader;
  }

  async showAlert(header: string, message: string, buttons: Array<any>, dismissible = true){
    const alert =  await  this.alertCtrl.create({
      mode: 'md',
      backdropDismiss: dismissible,
      header,
      message,
      buttons
    });
    await alert.present();
    return alert;
  }


  dataURIToBlob(dataURI: string): Blob {
    const splitDataURI = dataURI.split(',');
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++)
      ia[i] = byteString.charCodeAt(i);

    return new Blob([ia], { type: mimeString });
  }

}
