import { Component } from '@angular/core';

import { LocalNotificationsService } from './core/services/local-notifications.service';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private localNotification: LocalNotificationsService,
  ) {
    this.loadNotifications();
  }

  async loadNotifications() {
    try {
      if (Capacitor.isNativePlatform()) {
        this.localNotification.initLocalNotifications();
      }
    } catch (error) {
    console.log("⛔ ERROR ~ AppComponent ~ loadNotifications ~ error:", error)
    }
  }

}
