import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import * as dayjs from 'dayjs';

import { List, Task } from '../../shared/todo.interface';

@Component({
  selector: 'app-modal-datetime',
  templateUrl: './modal-datetime.component.html',
  styleUrls: ['./modal-datetime.component.scss'],
})
export class ModalDatetimeComponent implements OnInit {

  // ********************
  @Input("task") task: Task | undefined = undefined;
  // ********************
  today = dayjs().format();
  formDateTime = this.formBuilder.group({
    date_time: [ dayjs().format() ],
  });
  // ********************

  constructor(
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    if(this.task && this.task.to_date) {
      this.formDateTime.controls['date_time'].setValue(dayjs(this.task.to_date).format());
    }
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    const date_time = this.formDateTime.get('date_time')?.value;
    return this.modalCtrl.dismiss(date_time, 'confirm');
  }

}
