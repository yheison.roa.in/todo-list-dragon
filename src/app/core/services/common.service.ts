import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

import { GlobalService } from './globals.service';
import { Colors } from 'src/app/shared/colors.enum';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private globalService: GlobalService) {}

  // getStore(key: string): any {
  //   const value = localStorage.getItem(key);
  //   if (value) {
  //     return JSON.parse(value);
  //   } else {
  //     return null;
  //   }
  // }

  async getStore(key: string) {
    try {
      const { value } = await Preferences.get({
        key
      });
      if (!value) {
        return false
      } else {
        return JSON.parse(value)
      }
    } catch (error) {
      return false
    }
  }

  // setStore(key: string, value: any) {
  //   localStorage.setItem(key, JSON.stringify(value));
  // }
  async setStore(key: string, value: any) {
    await Preferences.set({
      key,
      value: JSON.stringify(value),
    });
  }

  async deleteStore(key: string) {
    // return localStorage.removeItem(key);
    await Preferences.remove({ key });
  }

  randomColor(color: any): string {
    const values = Object.keys(color);
    const enumKey = values[Math.floor(Math.random() * values.length)];
    return color[enumKey];
  }

  /*
  async setTodoList(user: any) {
    await Preferences.set({
      key: this.globalService.STORAGE_LOCAL_DATA,
      value: JSON.stringify(user),
    });
  }

  async getTodoList(keyValue: string){
    try {
      const { value } = await Preferences.get({
        key: this.globalService.STORAGE_LOCAL_DATA,
      });
      if (value) {
        return JSON.parse(value)
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
  */

}
