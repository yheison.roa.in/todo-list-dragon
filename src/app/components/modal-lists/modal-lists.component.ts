import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalController, PopoverController } from '@ionic/angular';
import { nanoid } from 'nanoid';

import { CommonService } from 'src/app/core/services/common.service';
import { GlobalService } from 'src/app/core/services/globals.service';
import { ColorsName } from 'src/app/shared/colors.enum';
import { List } from 'src/app/shared/todo.interface';
import { PopoverColorsComponent } from '../popover-colors/popover-colors.component';

@Component({
  selector: 'app-modal-lists',
  templateUrl: './modal-lists.component.html',
  styleUrls: ['./modal-lists.component.scss'],
})
export class ModalListsComponent  implements OnInit {

  // ********************
  lists: List[] = [];
  showNewList: boolean = false;
  // ********************
  selectedColor: string = 'primary';
  nameList: string = '';
  // ********************

  constructor(
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    public popoverController: PopoverController,
    private commonService: CommonService,
    private globalService: GlobalService,
  ) { }

  async ngOnInit() {
    this.lists = await this.commonService.getStore(this.globalService.STORAGE_LOCAL_TODO_LIST) || [];
    console.log("🚀 ~ ModalListsComponent ~ ngOnInit ~ lists:", this.lists)
  }

  async presentPopoverColor(e: Event) {
    const popover = await this.popoverController.create({
      component: PopoverColorsComponent,
      event: e,
    });

    await popover.present();

    const { data } = await popover.onDidDismiss();
    if(data) this.selectedColor = data;
  }

  selectList(list: List) {
    console.log("🚀 ~ ModalListsComponent ~ selectList ~ list:", list)
    return this.modalCtrl.dismiss(list, 'seleted_list');
  }

  newList() {
    const color_random: string = this.commonService.randomColor(ColorsName);
    this.selectedColor = color_random;
    this.showNewList = true;
  }

  async saveList() {
    this.showNewList = false;
    const new_list = {
      id: nanoid(),
      name: this.nameList,
      color_name: this.selectedColor || 'primary',
      task: 0,
      task_completed: 0
    };

    await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT, new_list);

    this.lists.push(new_list);
    await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST, this.lists);

    return this.modalCtrl.dismiss(new_list, 'seleted_list');
  }

}
