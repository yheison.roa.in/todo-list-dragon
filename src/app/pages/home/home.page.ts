import { Component, OnInit, ElementRef, QueryList, ViewChildren, ViewChild } from '@angular/core';
import { ActionSheetController, IonInput, ItemReorderEventDetail, ModalController } from '@ionic/angular';
import { nanoid } from 'nanoid';

import { List, Task } from '../../shared/todo.interface';
import { CommonService } from 'src/app/core/services/common.service';
import { GlobalService } from 'src/app/core/services/globals.service';
import { Colors, ColorsName } from 'src/app/shared/colors.enum';
import { ModalDatetimeComponent } from 'src/app/components/modal-datetime/modal-datetime.component';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalListsComponent } from 'src/app/components/modal-lists/modal-lists.component';
import { LocalNotificationsService } from 'src/app/core/services/local-notifications.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // ********************
  lists: List[] = [];
  list_current: List | undefined = undefined;
  todo_list_id: string = '';
  all_task: Task[] = [];
  task: Task[] = [];
  task_completed: Task[] = [];
  isDisabledReOrder = true;
  isModalDateHourOpen = false;
  isEditingList = false;
  // ********************
  btnReorderFill: string = 'clear';
  // ********************
  formListCurrent = this.formBuilder.group({
    name: [ '', Validators.required ],
    color: [ '', Validators.required ],
  });
  // ********************
  @ViewChildren('inputs') inputs!: QueryList<IonInput>;
  @ViewChildren('inputs_completed') inputs_completed!: QueryList<IonInput>;
  @ViewChildren('modalDate') modalDate!: ElementRef;
  @ViewChild('inputListName', { static: false }) inputListName?: IonInput;
  // ********************

  constructor(
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private formBuilder: FormBuilder,
    private localNotifications: LocalNotificationsService,
    private commonService: CommonService,
    private globalService: GlobalService,
  ) {
  }

  async ngOnInit() {
    this.lists = await this.commonService.getStore(this.globalService.STORAGE_LOCAL_TODO_LIST) || [];
    const list_current = await this.commonService.getStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT);
    if(list_current) this.list_current = list_current;
    else this.list_current = await this.newList('Mis Tareas');
    this.todo_list_id = `${this.globalService.STORAGE_LOCAL_TODO_LIST_TASKS}${this.list_current?.id}`;

    if(this.list_current) {
      this.formListCurrent.controls['name'].setValue(this.list_current.name);
      if(this.list_current.color) {
        this.formListCurrent.controls['color'].setValue(this.list_current.color);
        document.documentElement.style.setProperty('--ion-color-primary', `#${this.list_current.color}`);
      }
    }

    await this.getSaved();
  }

  async setDataListCurrent(list_current: List) {
    if(list_current) {
      await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT, list_current);
      this.list_current = list_current;
      this.todo_list_id = `${this.globalService.STORAGE_LOCAL_TODO_LIST_TASKS}${this.list_current?.id}`;

      this.formListCurrent.controls['name'].setValue(this.list_current.name);
      if(this.list_current.color) {
        this.formListCurrent.controls['color'].setValue(this.list_current.color);
        document.documentElement.style.setProperty('--ion-color-primary', `#${this.list_current.color}`);
      }
    }

    await this.getSaved();
  }

  async getSaved() {
    const all_tasks: Task[] = await this.commonService.getStore(this.todo_list_id);

    this.all_task = (all_tasks && all_tasks.length) ? all_tasks : [];
    if(this.all_task && this.all_task.length) {
      const tasks = this.all_task.filter((ele) => !ele.done);
      const task_completed = this.all_task.filter((ele) => ele.done);
      this.task = (tasks && tasks.length) ? this.orderTodoList(tasks) : [];
      this.task_completed = (task_completed && task_completed.length) ? this.orderTodoList(task_completed) : [];
    }
  }

  async newList(name: string, color?: string) {
    const color_random: string = this.commonService.randomColor(ColorsName);
    const new_list = {
      id: nanoid(),
      name,
      color_name: (color) ? color : color_random,
      task: 1,
      task_completed: 0
    };

    this.lists.push(new_list);

    const todo_list_id = `${this.globalService.STORAGE_LOCAL_TODO_LIST_TASKS}${new_list.id}`;

    const task = {
      id: nanoid(),
      list: todo_list_id,
      text: 'Nueva Tarea',
      order: 1
    };

    this.all_task.push(task);
    this.task.push(task);

    await this.commonService.setStore(todo_list_id, this.all_task);
    await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT, new_list);
    await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST, this.lists);

    return new_list;
  }

  async newToDo() {
    const task = {
      id: nanoid(),
      list: this.todo_list_id,
      text: '',
      order: this.task.length + 1
    };

    this.all_task.push(task);
    this.task.push(task);

    setTimeout(() => {
      this.inputs.last.setFocus();
    }, 500);
    // await this.saveTodoList();
  }

  async saveTodoList() {
    if(this.list_current){
      this.list_current.task = this.task.length || 0;
      this.list_current.task_completed = this.task_completed.length;
      await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT, this.list_current);
      console.log(this.list_current)
      const index_list = this.lists.findIndex((ele) => ele.id == this.list_current?.id);
      if(index_list >= 0) {
        this.lists[index_list].task = this.task.length || 0;
        this.lists[index_list].task_completed = this.task_completed.length;
        await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST, this.lists);
      }
    }

    return await this.commonService.setStore(this.todo_list_id, this.all_task);
  }

  orderTodoList(items: Task[]) {
    return [...items].sort((a, b) => (a.order || 0) - ( b.order || 0));
  }

  onInput(ev: any, id: string) {
    const value = ev.target!.value;
    const index_completed = this.task_completed.findIndex((ele) => ele.id == id);
    if(index_completed >= 0) this.task_completed[index_completed].text = value;

    const index = this.task_completed.findIndex((ele) => ele.id == id);
    if(index >= 0) this.task[index].text = value;

    const index_all = this.all_task.findIndex((ele) => ele.id == id);
    if(index_all >= 0) this.all_task[index_all].text = value;

    this.saveTodoList();
  }

  onCheck(ev: any, index: number) {
    /**** HACERLO POR ID */
    // const value = ev.detail.checked || false;
    // this.all_task[index].done = value;
    // this.task_completed.push(this.task[index]);
    // this.task.splice(index, 1);
    // this.saveTodoList();
  }

  toggleReorder() {
    this.isDisabledReOrder = !this.isDisabledReOrder;
    this.btnReorderFill = (this.isDisabledReOrder) ? 'clear' : 'solid';
  }

  handleReorder(ev: CustomEvent<ItemReorderEventDetail>) {
    // The `from` and `to` properties contain the index of the item
    // when the drag started and ended, respectively
    this.task[ev.detail.from].order = ev.detail.to;

    // Finish the reorder and position the item in the DOM based on
    // where the gesture ended. This method can also be called directly
    // by the reorder group
    ev.detail.complete();

    this.orderTodoList(this.task);
    this.saveTodoList();
  }

  async presentActionSheet(index: number) {
    const mark_readed = (!this.task[index].done) ? 'Marcar como completada' : 'Marcar como no completada';
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: mark_readed,
          data: {
            action: 'done',
          },
        },
        {
          text: 'Agregar Fecha/Hora',
          data: {
            action: 'date',
          },
        },
        {
          text: 'Eliminar',
          role: 'destructive',
          data: {
            action: 'delete',
          },
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          data: {
            action: 'cancel',
          },
        },
      ],
    });

    await actionSheet.present();

    const response = await actionSheet.onDidDismiss();
    if(response.data && response.data.action) {
      this.doAction(response.data.action, index);
    }
  }

  async doAction(action: string, index: number) {
    switch(action) {
      case 'done': {
        this.task[index].done = !this.task[index].done;
        this.saveTodoList();
        break;
      }
      case 'date': {
        this.toSetHour(index);
        break;
      }
      case 'delete': {
        this.deleteTask(index);
        break;
      }
    }
  }

  async toSetHour(index: number) {
    const modalDateTime = await this.modalCtrl.create({
      component: ModalDatetimeComponent,
      cssClass: 'modal-100',
      componentProps: {
        task: this.task[index]
      }
    });
    await modalDateTime.present();

    const response = await modalDateTime.onDidDismiss();
    if(response.data && response.role == 'confirm') {
      const date_time = response.data;
      this.task[index].to_date = date_time;

      // 🚧 Programar notificacion
      this.localNotifications.addLocalNotification({
        id: Date.now(),
        title: 'Recordatorio de Tarea',
        body: this.task[index].text,
        action: '',
        extraData: {
          list: this.list_current,
          task: this.task[index]
        }
      });
      await this.saveTodoList();
    }
  }

  async deleteTask(index: number) {
    this.task.splice(index, 1);
    await this.saveTodoList();
    // 🚧 Eliminar la notificacion programada
  }

  toEditList() {
    this.isEditingList = true;
    if(this.inputListName) this.inputListName.setFocus();
  }

  async toSaveList() {
    this.isEditingList = false;
    const name = (this.formListCurrent.get('name')?.value) || '';
    if(this.list_current) {
      this.list_current.name = name;
      this.list_current.task = this.task.length || 0;
      this.list_current.task_completed = this.task_completed.length;
      await this.commonService.setStore(this.globalService.STORAGE_LOCAL_TODO_LIST_CURRENT, this.list_current);
    }
  }

  async openLists() {
    const modal = await this.modalCtrl.create({
      component: ModalListsComponent,
      cssClass: 'modal-100',
      componentProps: {
        list_current: this.list_current
      }
    });
    await modal.present();

    const response = await modal.onDidDismiss();
    if(response.data && response.role == 'seleted_list') {
      this.setDataListCurrent(response.data);
    }
  }

  counterTask(list: List) {

  }

}
