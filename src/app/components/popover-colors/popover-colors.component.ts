import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ColorsName } from 'src/app/shared/colors.enum';

@Component({
  selector: 'app-popover-colors',
  templateUrl: './popover-colors.component.html',
  styleUrls: ['./popover-colors.component.scss'],
})
export class PopoverColorsComponent  implements OnInit {

  colors: string[] = [];

  constructor(
    public popoverController: PopoverController,
  ) { }

  ngOnInit() {
    const colors_arr = Object.values(ColorsName);
    for (const color of colors_arr) {
      this.colors.push(color)
    }
  }

  selectColor(color: string) {
  return this.popoverController.dismiss(color);

  }

}
