import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ModalDatetimeComponent } from './modal-datetime/modal-datetime.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { ModalListsComponent } from './modal-lists/modal-lists.component';
import { PopoverColorsComponent } from './popover-colors/popover-colors.component';

@NgModule({
  imports: [
    IonicModule.forRoot(),
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ModalDatetimeComponent,
    EditListComponent,
    ModalListsComponent,
    PopoverColorsComponent,
  ],
  exports: [
    ModalDatetimeComponent,
    EditListComponent,
    ModalListsComponent,
    PopoverColorsComponent,
  ],
})

export class ComponentsModule { }
