export interface List {
  id: string;
  name: string;
  color?: string;
  color_name?: string;
  task?: number;
  task_completed?: number;
  created_at?: Date;
  updated_at?: Date;
}

export interface Task {
  id: string;
  list: string;
  text: string;
  to_date?: Date | string;
  done?: boolean;
  order?: number;
  focus?: boolean;
  created_at?: Date;
  updated_at?: Date;
}
